const stripe = require("stripe")("sk_test_hdgqgipAg3eB6mqCiAuVxELT");


const db = require('../models/index');
const Product = db['Product'];
const Cart = db['Cart'];
const Order = db['Order'];


const ITEMS_PER_PAGE = 2;

exports.getProducts = (req, res, next) => {
  const page = parseInt(req.query.page) || 1;
  const offset = (page - 1) * ITEMS_PER_PAGE;
  let totalProducts;

  Product
    .count()
    .then(productsCount => {
      totalProducts = productsCount
      return Product
        .findAll({
          offset: offset,
          limit: ITEMS_PER_PAGE
        });
    })
    .then((products) => {
      res.render('shop/product-list', {
        prods: products,
        pageTitle: 'All Products',
        path: '/products',
        currentPage: page,
        hasNextPage: ITEMS_PER_PAGE * page < totalProducts,
        hasPreviousPage: page > 1,
        nextPage: page + 1,
        previousPage: page - 1,
        lastPage: Math.ceil(totalProducts / ITEMS_PER_PAGE)
      });
    })
    .catch((error) => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.getProduct = (req, res, next) => {
  const productId = req.params.productId;
  Product
    .findByPk(productId)
    .then(product => {
      res.render('shop/product-detail', {
        pageTitle: product.title,
        product: product,
        path: '/products'
      });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.getIndex = (req, res, next) => {
  const page = parseInt(req.query.page) || 1;
  const offset = (page - 1) * ITEMS_PER_PAGE;
  let totalProducts;

  Product
    .count()
    .then(productsCount => {
      totalProducts = productsCount
      return Product
        .findAll({
          offset: offset,
          limit: ITEMS_PER_PAGE
        });
    })
    .then(products => {
      res.render('shop/index', {
        prods: products,
        pageTitle: 'Shop',
        path: '/',
        currentPage: page,
        hasNextPage: ITEMS_PER_PAGE * page < totalProducts,
        hasPreviousPage: page > 1,
        nextPage: page + 1,
        previousPage: page - 1,
        lastPage: Math.ceil(totalProducts / ITEMS_PER_PAGE)
      });
    })
    .catch((error) => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.getCart = (req, res, next) => {
  Cart
    .findOne({
      where: {
        userId: req.session.user.id
      }
    })
    .then(cart => {
      return cart.getProducts();
    })
    .then(products => {
      res.render('shop/cart', {
        path: '/cart',
        pageTitle: 'Your Cart',
        products: products
      });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.postCartDeleteProduct = (req, res, next) => {
  const productId = req.body.productId;

  Cart
    .findOne({
      where: {
        userId: req.session.user.id
      }
    })
    .then(cart => {
      fetchedCart = cart;
      return cart.getProducts({
        where: {
          id: productId
        }
      });
    })
    .then(products => {
      let product = products[0];
      return product.CartItem.destroy();
    })
    .then(result => {
      res.redirect('/cart');
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
}

exports.postCart = (req, res, next) => {
  const productId = req.body.productId;
  let fetchedCart;
  let newQuantity = 1;

  Cart
    .findOne({
      where: {
        userId: req.session.user.id
      }
    })
    .then(cart => {
      fetchedCart = cart;
      return cart.getProducts({
        where: {
          id: productId
        }
      });
    })
    .then(products => {
      let product;
      if (products.length > 0) {
        product = products[0];
      }

      if (product) {
        const oldQuantity = product.CartItem.quantity;
        newQuantity = oldQuantity + 1;
        return product;
      }

      return Product.findByPk(productId);
    })
    .then(product => {
      return fetchedCart.addProduct(product, {
        through: {
          quantity: newQuantity
        }
      });
    })
    .then(cart => {
      res.redirect('/cart');
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.postOrder = (req, res, next) => {

  const token = req.body.stripeToken;
  let totalSum = 0;
  let order;
  let fetchedCart;
  Cart
    .findOne({
      where: {
        userId: req.session.user.id
      }
    })
    .then(cart => {
      fetchedCart = cart;
      return cart.getProducts();
    })
    .then(products => {
      products.forEach(product => {
        totalSum += product.price * product.CartItem.quantity;
      });

      order = new Order({
        userId: req.session.user.id,
      });

      const orderProducts = products.map((product) => {
        product.OrderItem = {
          quantity: product.CartItem.quantity
        }
        return product;
      });

      return order.addProducts(orderProducts);
    })
    .then(result => {
      return order.save();
    })
    .then(order => {
      return fetchedCart.setProducts(null);
    })
    .then(result => {
      const charge = stripe.charges.create({
        amount: totalSum * 100,
        currency: 'kes',
        description: 'Your Order',
        source: token,
        metadata: {
          order_id: order.id.toString()
        }
      });
      res.redirect('/orders');
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    })
};

exports.getOrders = (req, res, next) => {

  Order
    .findAll({
      where: {
        userId: req.session.user.id
      },
      include: ['Products']
    })
    .then(orders => {
      res.render('shop/orders', {
        path: '/orders',
        pageTitle: 'Your Orders',
        orders: orders
      });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    })

};

exports.getCheckout = (req, res, next) => {
  Cart
    .findOne({
      where: {
        userId: req.session.user.id
      }
    })
    .then(cart => {
      return cart.getProducts();
    })
    .then(products => {
      let totalSum = 0;
      products.forEach(product => {
        totalSum += product.price * product.CartItem.quantity;
      });

      res.render('shop/checkout', {
        path: '/checkout',
        pageTitle: 'Checkout',
        products: products,
        totalSum: totalSum
      });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

