const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const db = require('../models/index')
const utilityFunctions = require('../util/utility-functions');
const {validationResult} = require('express-validator/check')

const User = db['User'];

exports.getLogin = (req, res, next) => {
  renderLoginPage(req, res);
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return renderLoginPage(
      req,
      res,
      errors.array()[0].msg,
      errors.array());
  }

  User
    .findOne({
      where: {
        email: email
      }
    })
    .then(user => {
      if (!user) {
        const errorMessage = 'Invalid email or password';
        const validationErrors = [
          {
            param: 'email'
          },
          {
            param: 'password'
          }];

        return renderLoginPage(
          req,
          res,
          errorMessage,
          validationErrors);
      }

      bcrypt
        .compare(password, user.password)
        .then(passwordsMatch => {
          if (passwordsMatch !== true) {
            const errorMessage = 'Invalid email or password';
            const validationErrors = [
              {
                param: 'email'
              },
              {
                param: 'password'
              }];

            return renderLoginPage(
              req,
              res,
              errorMessage,
              validationErrors);

          } else {
            req.session.isAuthenticated = true;
            req.session.user = user;
            // N.B. We don't need to save but we want to ensure that
            // we only redirect after the session has completed saving
            req.session.save((error) => {
              if (error) {
                console.log(error);
              }
              res.redirect('/');
            });
          }
        })
        .catch(error => {
          console.log(error);
          res.redirect('/');
        })
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.postLogout = (req, res, next) => {
  req.session.destroy((error) => {
    if (error) {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    } else {
      res.redirect('/');
    }
  })

};

exports.getSignup = (req, res, next) => {
  let messages = req.flash('error');
  let message;
  if (messages.length > 0) {
    message = messages[0];
  }
  res.render('auth/signup', {
    path: '/signup',
    pageTitle: 'Signup',
    errorMessage: message,
    oldInput: {
      email: '',
      password: '',
      confirmPassword: ''
    },
    validationErrors: []
  });
};

exports.postSignup = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res
      .status(422)
      .render('auth/signup', {
        path: '/signup',
        pageTitle: 'Signup',
        errorMessage: errors.array()[0].msg,
        oldInput: {
          email: email,
          password: password,
          confirmPassword: confirmPassword
        },
        validationErrors: errors.array()
      });
  }

  bcrypt
    .hash(password, 12)
    .then(hashedPassword => {
      return User.create({
        name: email,
        email: email,
        password: hashedPassword
      });
    })
    .then(user => {
      return user.createCart();
    })
    .then(result => {
      res.redirect('/login');
      // Send email. (we'll not wait for it to complete before redirecting at the moment)
      return utilityFunctions
        .sendEmail(email, 'Signup succeeded!', '<h1>You successfully signed up</h1>')
        .then((result) => {
          console.log(result);
        }).catch(error => {
        console.log(error)
      });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.getReset = (req, res, next) => {
  res.render('auth/reset', {
    path: '/reset',
    pageTitle: 'Reset Password',
  });
};

exports.postReset = (req, res, next) => {
  const email = req.body.email;

  if (!email) {
    req.flash('error', 'Email address is required!');
    return res.redirect('/reset');
  }

  // generate token
  crypto.randomBytes(32, (error, buffer) => {
    if (error) {
      req.flash('error', 'Errors occurred while resetting password!');
      return res.redirect('/reset');
    }

    const token = buffer.toString('hex');

    User
      .findOne({
        where: {
          email: email
        }
      })
      .then(user => {
        if (!user) {
          req.flash('error', 'No account with that email found!');
          return res.redirect('/reset');
        }

        user.resetToken = token;
        user.resetTokenExpiration = Date.now() + 3600000;

        user.save()
          .then(result => {
            // Send Email
            res.redirect('/login');
            utilityFunctions
              .sendEmail(
                email,
                'Password Reset',
                `
                <p>You requested a password reset</p>
                <p>Click <a href="http://localhost:3000/change-password/${token}">this link</a> to set a new password.</p>
                `)
              .then((result) => {
                console.log(result);
              })
              .catch(error => {
                console.log(error)
              });

          });

      })
      .catch(error => {
        const err = new Error(error);
        err.httpStatusCode = 500;
        next(err);
      });

  })
};

exports.getChangePassword = (req, res, next) => {
  const token = req.params.token;
  User
    .findOne({
      where: {
        resetToken: token,
        resetTokenExpiration: {
          $gt: Date.now()
        }
      }
    })
    .then(user => {
      if (!user) {
        return;
      }

      res.render('auth/change-password', {
        path: '/change-password',
        pageTitle: 'Change Password',
        userId: user.id,
        token: token
      });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });


};

exports.postChangePassword = (req, res, next) => {
  const password = req.body.password;
  const confirmPassword = req.body.password;
  const userId = req.body.userId;
  const token = req.body.token;

  let errorMessage = "";

  if (!password || password.trim().length == 0) {
    errorMessage = errorMessage.length == 0 ? errorMessage : errorMessage + '\nPassword is required.';
  }

  if (!confirmPassword || confirmPassword.trim().length == 0) {
    errorMessage = errorMessage.length == 0 ? errorMessage : errorMessage + '\nPassword confirmation is required.';
  }

  if (password && password.trim().length > 0 && confirmPassword && confirmPassword.trim().length > 0) {
    if (password !== confirmPassword) {
      errorMessage = errorMessage.length == 0 ? errorMessage : errorMessage + '\nPassword and Password Confirmation are required.';
    }
  }

  if (errorMessage.length > 0) {
    req.flash('error', errorMessage);
    return res.redirect('/change-password/' + token);
  }

  let resetUser;
  User
    .findOne({
      where: {
        id: userId,
        resetToken: token,
        resetTokenExpiration: {
          $gt: Date.now()
        }
      }
    })
    .then(user => {
      resetUser = user;
      return bcrypt.hash(password, 12);
    })
    .then(hashedPassword => {
      resetUser.password = hashedPassword;
      resetUser.resetToken = null;
      resetUser.resetTokenExpiration = null;

      return resetUser.save()
    })
    .then(result => {

      res.redirect('/login');
      utilityFunctions
        .sendEmail(resetUser.email, 'Password successfully reset.', '<p>Your password has been successfully reset</p>')
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};


// Private functions
renderLoginPage = (req, res, errorMessage, validationErrors) => {
  if (errorMessage) {
    return res
      .status(422)
      .render('auth/login', {
        path: '/login',
        pageTitle: 'Login',
        errorMessage: errorMessage,
        oldInput: {
          email: req.body.email,
          password: req.body.password
        },
        validationErrors: validationErrors
      });
  } else {
    return res.render('auth/login', {
      path: '/login',
      pageTitle: 'Login',
      errorMessage: '',
      oldInput: {
        email: '',
        password: ''
      },
      validationErrors: []
    });
  }
}