const db = require('../models/index');
const Product = db['Product'];
const {validationResult} = require('express-validator/check')
const ITEMS_PER_PAGE = 2;

exports.getAddProduct = (req, res, next) => {
  res.render('admin/edit-product', {
    pageTitle: 'Add Product',
    path: '/admin/add-product',
    editing: false,
    errorMessage: '',
    oldInput: {
      title: '',
      imageUrl: '',
      price: '',
      description: ''
    },
    validationErrors: []
  });
};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422)
      .render('admin/edit-product', {
        pageTitle: 'Add Product',
        path: '/admin/add-product',
        editing: false,
        errorMessage: errors.array()[0].msg,
        oldInput: {
          title: title,
          imageUrl: imageUrl,
          price: price,
          description: description
        },
        validationErrors: errors.array()
      });
  }

  Product
    .create({
      title: title,
      price: price,
      imageUrl: imageUrl,
      description: description,
      userId: req.session.user.id
    })
    .then((result) => {
      res.redirect('/admin/products');
    })
    .catch((error) => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;
  if (!editMode) {
    return res.redirect('/');
  }

  const productId = req.params.productId;

  Product
    .findByPk(productId)
    .then((product) => {
      if (!product) { // todo => refactor this
        return res.redirect('/');
      }

      res.render('admin/edit-product', {
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        editing: editMode,
        product: product,
        errorMessage: '',
        oldInput: {
          title: '',
          imageUrl: '',
          price: '',
          description: ''
        },
        validationErrors: []
      });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};

exports.postEditProduct = (req, res, next) => {

  const id = req.body.productId;
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422)
      .render('admin/edit-product', {
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        editing: true,
        errorMessage: errors.array()[0].msg,
        product: {
          id: id,
          title: title,
          imageUrl: imageUrl,
          price: price,
          description: description
        },
        oldInput: {
          title: title,
          imageUrl: imageUrl,
          price: price,
          description: description
        },
        validationErrors: errors.array()
      });
  }

  Product.findByPk(id)
    .then(product => {
      if (product.userId !== req.session.user.id) {
        return res.redirect('/');
      }

      product.title = title;
      product.imageUrl = imageUrl;
      product.price = price;
      product.description = description;
      return product
        .save()
        .then(result => {
          res.redirect('/admin/products');
        });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    })

};

exports.deleteProduct = (req, res, next) => {
  const productId = req.params.productId;

  Product
    .destroy({
      where: {
        id: productId,
        userId: req.session.user.id
      }
    })
    .then(result => {
      res.status(200).json({
        message: "Success!"
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Deleting product failed"
      });
    });
};

exports.getProducts = (req, res, next) => {
  const page = parseInt(req.query.page) || 1;
  const offset = (page - 1) * ITEMS_PER_PAGE;
  let totalProducts;

  Product
    .count({
      where: {
        userId: req.session.user.id
      }
    })
    .then(productsCount => {
      totalProducts = productsCount
      return Product
        .findAll({
          where: {
            userId: req.session.user.id
          },
          offset: offset,
          limit: ITEMS_PER_PAGE
        });
    })
    .then((products) => {
      res.render('admin/products', {
        prods: products,
        pageTitle: 'Admin Products',
        path: '/admin/products',
        currentPage: page,
        hasNextPage: ITEMS_PER_PAGE * page < totalProducts,
        hasPreviousPage: page > 1,
        nextPage: page + 1,
        previousPage: page - 1,
        lastPage: Math.ceil(totalProducts / ITEMS_PER_PAGE)
      });
    })
    .catch(error => {
      const err = new Error(error);
      err.httpStatusCode = 500;
      next(err);
    });
};
