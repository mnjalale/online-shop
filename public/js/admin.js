const deleteProduct = (btn) => {
  let productId = btn.parentNode.querySelector('[name=productId]').value;
  let csrfToken = btn.parentNode.querySelector('[name=_csrf]').value;
  let productElement = btn.closest('article');

  fetch('/admin//product/' + productId, {
    method: 'DELETE',
    headers: {
      'csrf-token': csrfToken
    }
  })
    .then(result => {
      return result.json();
    })
    .then(data => {
      console.log(data);
      productElement.parentNode.removeChild(productElement);
    })
    .catch(error => {
      console.log(error);
    })
}