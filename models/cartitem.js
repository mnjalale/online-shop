'use strict';
module.exports = (sequelize, DataTypes) => {
  const CartItem = sequelize.define('CartItem', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    quantity: {
      type: DataTypes.INTEGER
    }
  }, {});
  CartItem.associate = function(models) {
    // associations can be defined here    
  };
  return CartItem;
};