'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    // todo => consider await/async for better indentation/readablilty
    let sequelize = queryInterface.sequelize;
    return sequelize.transaction((transaction) => {
      return queryInterface
        .addColumn(
          'Users',
          'resetToken',
          {
            type: Sequelize.STRING,
          },
          {
            transaction: transaction
          })
        .then(() => {
          return queryInterface
            .addColumn(
              'Users',
              'resetTokenExpiration',
              {
                type: Sequelize.DATE
              },
              {
                transaction: transaction
              }
          );
        })
    });

  },

  down: (queryInterface, Sequelize) => {
    let sequelize = queryInterface.sequelize;
    return sequelize.transaction((transaction) => {

      return queryInterface
        .removeColumn(
          'Users',
          'resetTokenExpiration',
          {
            transaction: transaction
          })
        .then(() => {
          return queryInterface
            .removeColumn(
              'Users',
              'resetToken',
              {
                transaction: transaction
              }
          )
        })
    });
  }
};
