'use strict';
const entities = require('../util/models');
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CartItems', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      cartId: {
        type: Sequelize.UUID,
        onDelet: 'CASCADE',
        references: {
          model: entities.Cart,
          key: 'id'
        }
      },
      productId: {
        type: Sequelize.UUID,
        onDelete: 'CASCADE',
        references: {
          model: entities.Product,
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CartItems');
  }
};