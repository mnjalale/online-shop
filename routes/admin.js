const express = require('express');

const adminController = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');
const adminValidator = require('../middleware/routeValidators/admin');

const router = express.Router();

// /admin/add-product => GET
router.get('/add-product', isAuth, adminController.getAddProduct);

router.get('/edit-product/:productId', isAuth, adminController.getEditProduct);

// /admin/products => GET
router.get('/products', isAuth, adminController.getProducts);

// /admin/add-product => POST
router.post('/add-product', isAuth, adminValidator.validateAddProduct, adminController.postAddProduct);

router.post('/edit-product', isAuth, adminValidator.validateEditProduct, adminController.postEditProduct);

router.delete('/product/:productId', isAuth, adminController.deleteProduct)

module.exports = router;
