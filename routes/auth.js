const express = require('express');
const authValidator = require('../middleware/routeValidators/auth');

const authController = require('../controllers/auth');


const router = express.Router();


router.get('/login', authController.getLogin);

router.post('/login', authValidator.validateLogin, authController.postLogin);

router.post('/logout', authController.postLogout);

router.get('/signup', authController.getSignup);

router.post('/signup', authValidator.validateSignup, authController.postSignup);

router.get('/reset', authController.getReset);

router.post('/reset', authController.postReset);

router.get('/change-password/:token', authController.getChangePassword);

router.post('/change-password', authController.postChangePassword);

module.exports = router;

