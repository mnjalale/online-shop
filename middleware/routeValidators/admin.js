const {check, body} = require('express-validator/check');

exports.validateAddProduct = [
  body('title')
    .not().isEmpty().withMessage('Title is required')
    .isLength({
      min: 3
    }).withMessage('Title should be at least 3 characters long'),
  body('imageUrl')
    .not().isEmpty().withMessage('Image URL is required')
    .isURL().withMessage('Enter valid image URL'),
  body('price')
    .not().isEmpty().withMessage('Price is required')
    .isFloat().withMessage('Enter valid price'),
  body('description')
    .isLength({
      max: 200
    })
    .withMessage('Description should have a maximum length of 200')
];

exports.validateEditProduct = [
  body('title')
    .not().isEmpty().withMessage('Title is required')
    .isLength({
      min: 3
    }).withMessage('Title should be at least 3 characters long'),
  body('imageUrl')
    .not().isEmpty().withMessage('Image URL is required')
    .isURL().withMessage('Enter valid image URL'),
  body('price')
    .not().isEmpty().withMessage('Price is required')
    .isFloat().withMessage('Enter valid price'),
  body('description')
    .isLength({
      max: 200
    })
    .withMessage('Description should have a maximum length of 200')
];