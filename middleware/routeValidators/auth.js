const {check, body} = require('express-validator/check');
const db = require('../../models/index');
const User = db['User'];

exports.validateSignup = [
  body('email')
    .not().isEmpty().withMessage('Email address is required')
    .isEmail().withMessage('Enter valid email address')
    .custom((value, {req}) => {
      return User
        .findOne({
          where: {
            email: value
          }
        })
        .then(user => {
          if (user) {
            return Promise.reject('The provided email is already in use.');
          }
        });
    })
    .normalizeEmail(),
  body('password')
    .not().isEmpty().withMessage('Password is required')
    .isLength({
      min: 6
    }).withMessage('Password must be at least 6 characters long')
    .custom((value, {req}) => {
      if (value !== req.body.confirmPassword) {
        throw Error('Password does not match password confirmation')
      } else {
        return true;
      }
    }),
  body('confirmPassword')
    .not().isEmpty().withMessage('Password confirmation is required')
];

exports.validateLogin = [
  body('email')
    .not().isEmpty().withMessage('Email address is required').normalizeEmail()
    .isEmail().withMessage('Enter valid email address'),
  body('password')
    .not().isEmpty().withMessage('Password is required')
];